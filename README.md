## **Prueba de maquetación para Aguayo Publicidad S.A.S**
Pagina basada en el diseño entregado en figma , con temática de diseño para la empresa Avianca.

 - **Responsive:**
	 - Primer Breakpoint :320px Movil 📱
	 - Segundo Breakpoint: 700px Tablet
	 - Tercer Breakpoint: 1000px Web 🖥️
 - **Menú**

    &nbsp; &nbsp;  &nbsp; &nbsp; Es menú desplegable hasta los 700px,ademas es fijo y se mueve con el flujo de la pagina.
 - **Formulario de Vuelos**

    &nbsp; &nbsp;  &nbsp; &nbsp; En este formulario los campos de las ciudades funcionan como select y search.
 - **Carrusel**

    &nbsp; &nbsp;  &nbsp; &nbsp; El carrusel es funcional hacia cualquier dirección(derecha o izquierda).
 - **Tratamiento de Imágenes**

     &nbsp; &nbsp;  &nbsp; &nbsp; Se comprimieron los assets un 70% para mejorar la velocidad de carga.
